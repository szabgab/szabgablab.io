# szabgab.Gitlab.Io

This is the source code of [szabgab.gitlab.io](https://szabgab.gitlab.io/).

Detailed desciption of [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)

## Static HTML page

Create a repository `USERNAME.gitlab.io`. (Because my GitLab username is szabgab so I created a repository called `szabgab.gitlab.io` )
It can be a private repository as well.
In the repository create a directory called `public` and inside a file called `index.html` with whatever you'd like to have on your main page.

Also create a file called `.gitlab-ci.yml` in the root of the project with the following content:

```
pages:
  script: echo hi
  artifacts:
    paths:
      - public
```

Commit and push. It will run the CI job and after a few seconds, maybe 30-60, you'll see the content of your index.html page on your GitLab page:
In my case it is [szabgab.gitlab.io](https://szabgab.gitlab.io/).


## Pages of Other repositories

If you have another reposiotry, e.g  called `my-repo` and if you create GitLab pages with this repo (that is you set up the appropriate job in .gitlab-ci.yml
and have file in the public/ directory), thos pages will show up in the `USERNAME.gitlab.io/my-repo/` folder.



